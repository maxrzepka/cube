$(function(){
    var cube = window.cube = {
        model: {},
        view: {},
        initialize: function(){
            var sources = new cube.model.SourceCollection();
            var view1 = new SourceCollectionView({model: sources});
        }
    };

    var Source = cube.model.Source = Backbone.Model.extend({
        url: function(){
            return  'source/' + this.get('code');
        }
    });

    //{header : [], rows[[],[]..]}
    var ResultData = cube.model.ResultData = Backbone.Model.extend({
        url :function(){
            return "query/" + this.get('id') + "/data";
        },

        //build query (filter) corresponding to a row (make sense only for aggregate row)
        //get all dimensions values
        row_filter: function(row){
            var filt = {};
            var self = this;
            _.each(row,function(e,i){
                if( self.get('type')[i] == "dimension" ){
                    filt[self.get('field')[i]] = e;
                }
            });
            return filt;
        },
    });

    //attrs : row and corresponding filter query if aggregate row
    var DataRow = cube.model.DataRow = Backbone.Model.extend({

    });

    var Query = cube.model.Query = Backbone.Model.extend({

        url: function(){
            if( this.get('id') ){
                return 'query/' + this.get('id');
            }else{
                return 'query';
            }
        },

        setFilter: function(attrs){
            var filt = this.get('filter') || {};
            //    || this.set('filter',new Backbone.Model.extend({})).get('filter');
            for( a in attrs){
                filt[a] = attrs[a];
            }
            this.unset('id');//need to save again after change
            return this.set('filter',filt);
        },

        setAggregate: function(fields){
            return this.set('aggregate',fields);
        },
    });

    var QueryExport = cube.model.QueryExport = Backbone.Model.extend({
        url: function(){
            if( this.get('id') ){
                return "report/" +  this.get('id');
            }else{
                return "query/" + this.get('query') + "/export";
            }
        },
    });

    //[source: "",dimension "",values []]
    var FieldValues = cube.model.FieldValues = Backbone.Model.extend({
        initialize: function(attrs){
            this.url = "source/" + attrs.source + "/values/" + attrs.dimension;
        },
    });

    var FieldValuesCollection = cube.model.FieldValuesCollection
        = Backbone.Collection.extend({
            model: FieldValues,
            initialize: function(attrs){
                this.url = "source/" + attrs.source + "/values";
                this.deferred = this.fetch();
            },
        });
    //fields + id
    //data attribute obtained from query/:id/data , not found = not yet available
    var Aggregate = cube.model.Aggregate = Backbone.Model.extend({

        //a model object need to be fetch
        data: function(){
            console.log("Fetch data for " + this.get('code'));
            return new cube.model.ResultData({id:  this.get('code')
                                              ,source: this.get('source')});
        },

    });

    var AggregateCollection = cube.model.AggregateCollection
                            = Backbone.Collection.extend({
        model: Aggregate,
        //url: 'source',

        initialize: function(attrs){
            if( attrs && attrs.source ){
                this.url = 'source/' + attrs.source.get('code') + '/aggregates';
            }
        }
    });

    var SourceCollection = cube.model.SourceCollection = Backbone.Collection.extend({
        model: Source,
        url: 'source',
    });

    var RowView = cube.view.RowView = Backbone.View.extend({
        //model: DataRow,
        tagName: "tr",
        events : { "click" : "showDetails"},
        tpltable: _.template($('#result-table').html()),
        render: function(){
            var self = this;
            _.each(this.model.row,function(e,i){
                $(self.el).append($("<td>").addClass("number").html(e));
            })
            return this;
        },

        //copy of showData only HTML target change
        showDetails: function(){
            var self = this;
            var query = new cube.model.Query({filter : this.model.filter
                                              ,source : this.model.source});
            query.save().done(
                function(){
                    var data = new cube.model.ResultData({id:  query.get('id')});
                    data.fetch({
                        success: function() {
                            $("#details-table").html(self.tpltable(data.toJSON()));
                        },
                        error: function() {
                            //TODO trigger request to fetch query data
                            $("#details-table").html("Query " + query.get('id')
                                            + " not found "
                                            + "<a class=\"btn\">Try again</a>");
                        //console.log(self.model.get('source') + " not found");
                        },
                    });
                }
            );
        },
    });

    var DataTableView = cube.view.DataTableView = Backbone.View.extend({
        //el: $("#aggregate-table"),
        model: ResultData,
        tagName: "table",
        template: _.template($("#table-header").html()),
        render: function(){
            var self = this;
            var classes = _.map(this.model.type,function(t){
                if( t == "measure"){ return "number";}else{ return "";}
            });
            //header part
            $(this.el).addClass("table table-bordered table-hover")
                .append(this.template({header: this.model.get('header')}));
            //rows
            _.each(this.model.get('rows'),function (row){
                var filt = self.model.row_filter(row);
                var rview = new RowView({model: {row: row,filter: filt
                                                 ,type: classes
                                                 ,source : self.model.get('source')}});
                $(self.el).append(rview.render().el);
            });

            return this;
        }
    });

    var FilterDropdown = cube.view.FilterDropdown = Backbone.View.extend({
        template: _.template($("#filter-widget").html()),
        model: FieldValues,

        render: function(){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
    });


    //Filter Panel of a source : filter drop downs buttons + data
    var FilterPanel = cube.view.FilterPanel = Backbone.View.extend({
        el: $("#filter-panel"),
        template: _.template($("#filter-toolbar").html()),
        tpltable: _.template($('#result-table').html()),
        model: Query,
        events: {
            "change select" : "changeFilter",
            "click a.filter" : "executeFilter",
            "click a.export" : "showExport",
        },
        initialize: function(attrs){
            this.source = attrs.source;
            var code = attrs.source.get('code');
            this.model = new Query({source : code});
            this.filters = new FieldValuesCollection({source: code});
            _.bindAll(this,'render');
            this.filters.deferred.done(this.render);
        },
        render: function(){
            console.log("render Filter panel ");
                //TODO check what in arguments
                this.$(".control-group")
                    .html(this.template(this.source.toJSON()));
                var that = this;
                this.filters.each(function(filt){
                    var view = new cube.view.FilterDropdown({model: filt});
                    that.$(".control-group").append(view.render().el);
                });
                this.$(".control-group").append("<a class=\"btn filter\">Filter</a>");
            return this;
        },

        changeFilter: function(e){
            var field = $(e.currentTarget);
            var values = _.map($("option:selected", field),function(e){return e.value;});
            var data = {};
            if( values.length == 0 ){
                data[field.attr('name')] = [];
            }else if( values.length == 1 ){
                data[field.attr('name')] = values[0];
            }else if( values.length > 1 ){
                data[field.attr('name')] = values;
            }
            this.model.setFilter(data);
            //this.search = {$(e).attr('name'): $(e).val()};
        },
        executeFilter: function(e){
            var self = this;
            console.log("executeFilter ");
            if( this.model.get('id') ){
                this.showData();
            }else{
                var d = this.model.save();
                d.done(function(){
                        self.showData();
                    }
                );
                console.log("filter saved : " + d.state());
            }
        },

        showExport: function(){
            var exportView =
                new ExportModal({model: this.model});
            exportView.render();

            var $modal = $("#modal");
            $modal.html(exportView.el);
            $("#modal .modal").modal();
        },

        showData: function(){
            var self = this;
            var data = new ResultData(self.model);
            data.fetch({
                success: function() {
                    var exportButton = $("<a>").addClass("btn export").html("Export");
                    $("#filter-table").html(self.tpltable(data.toJSON()))
                        .append(exportButton);
                },
                error: function() {
                    $("#filter-table").html("Query " + self.model.get('id')
                                            + " not found "
                                            + "<a class=\"btn filter\">Try again</a>");
                    //console.log(self.model.get('source') + " not found");
                },
            });
        },
    });

    var AggregateButton = cube.view.AggregateButton = Backbone.View.extend({
        template: _.template($('#aggregate-button').html()),
        tpltable: _.template($('#result-table').html()),
        events: {"click" : "showData",
                 "click a.btn" : "showData"
                },

        render: function(){
            //console.log("render aggregate : " + this.model.get('code'));
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        showData: function(){
            var data = this.model.data();
            var self = this;
            //Wait until data loaded
            data.fetch({
                success: function() {
                    //$("#aggregate-table").html(self.tpltable(data.toJSON()));
                    var view = new DataTableView({model: data
                                                  ,source: self.model.get('source')});
                    $("#aggregate-table").html(view.render().el);
                },
                error: function() {
                    $("#aggregate-table").html("Query " + this.model.get('code')
                                               + " not found "
                                              + "<a>Try again</a>");
                    console.log(this.model.get('code') + " not found");
                },
            });
        }
    });

    var ExportModal = cube.view.ExportModal = Backbone.View.extend({
        template: _.template($("#export-modal").html()),
        model: Query,
        events: {"click a.btn-primary" : "exportData"},

        render: function(){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        exportData: function(){
            console.log("Export data of query " + this.model.get('id'));
            var queryExport = new QueryExport({query: this.model.get('id')});
            //TODO extract infos from HTML form
            var that = this;
            var d = queryExport.save();
                    var exportlink =
                    $("<a>").attr("href",queryExport.url())
                        .attr("target","_blank")
                        .html("Download");
                    $("a.btn-primary",that.$el).after(exportlink);
        },
    });

    //Aggregate Panel of a source : aggregate buttons + data
    var AggregatePanel = cube.view.AggregatePanel = Backbone.View.extend({
        //el: $("#aggregate-panel"),
        template: _.template($("#aggregate-toolbar").html()),
        initialize: function(attrs){
            this.aggregates = new AggregateCollection({source: attrs.source});
            //_.bindAll(this,'addAggregate','addAggregates');
            this.aggregates.on('add', this.addAggregate, this);
            this.aggregates.on('reset', this.addAggregates, this);

            this.source = attrs.source;

            $("#aggregate-panel .btn-toolbar")
                .html(this.template(this.source.toJSON()));

            this.aggregates.fetch();
        },

        addAggregate: function(aggregate){
            var view = new cube.view.AggregateButton({model: aggregate});
            //view.on("click",);
            $("#aggregate-panel .btn-group").append(view.render().el);
        },

        addAggregates: function(){
            console.log("AddAggregates " + this.aggregates.length + " elements");
            this.aggregates.each(this.addAggregate);
        },
    });

    var SourceView = cube.view.SourceView = Backbone.View.extend({
        tagName: "li",
        template: _.template($('#source-template').html()),
        events: { "click a.aggregate" : "viewAggregate"
                  ,"click a.filter" : "viewFilter"
                },
        render: function() {
            console.log("render source : " + this.model.get('name'));
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
        },

        //TODO cache current aggregates
        viewAggregate: function(el){
            el.preventDefault();
            console.log("viewAggregate of " + this.model.get('name'));
            this.panel = new AggregatePanel({source: this.model});
        },

        viewFilter: function(el){
            el.preventDefault();
            console.log("viewFilter of " + this.model.get('name'));
            this.panel = new FilterPanel({source: this.model});
        }
    });

    var SourceCollectionView = cube.view.SourceCollectionView = Backbone.View.extend({
        el: $("#source-list"),

        initialize: function(){
            _.bindAll(this,'render','addAll','addOne');
            this.model.on('add', this.addOne, this);
            this.model.on('reset', this.addAll, this);
            this.model.on('all', this.render, this);

            this.model.fetch();
        },

        render: function(){
            $(this.el).after( this.model.length + " Sources found ");
        },
        addOne: function(source) {
            console.log("AddOne Source " + source.get('name'));
            var view = new cube.view.SourceView({model: source});
            $(this.el).append(view.render().el);
        },

        addAll: function() {
            console.log("AddAll Sources " + this.model.length + " elements");
            this.model.each(this.addOne);
        },


    });
})