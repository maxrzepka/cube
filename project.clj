(defproject cube "0.1.0-SNAPSHOT"
  :description "Data Analysis Made Simple"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 ;;data stuff
                 [cascalog "1.10.0"]
                 [cascading/cascading-local "2.0.0"]
                 [org.clojure/data.csv "0.1.2"]
                 [org.clojure/java.jdbc "0.2.3"]
                 ;; [com.datomic/datomic-free "0.8.3848"
                 ;; :exclusions [org.slf4j/slf4j-nop org.slf4j/log4j-over-slf4j]]
                 ;;web stuff
                 [enlive "1.0.1"]
                 [ring "1.1.6"]
                 [net.cgrand/moustache "1.1.0"]
                 [ring-middleware-format "0.2.0"]
                 ;;misc
                 [org.clojure/tools.cli "0.2.2"]
                 [sonian/carica "1.0.2" :exclusions [[cheshire]]]
                 ;;need to get hadoop class
                 [org.apache.hadoop/hadoop-core "0.20.2-dev"]
                 ]
  :main cube.cli
  ;;to get cascading-local
  :repositories {"conjars.org" "http://conjars.org/repo"}
  :profiles {:dev {:dependencies [[mysql/mysql-connector-java "5.1.6"]
                                  [hsqldb/hsqldb "1.8.0.10"]
                                  ;; [org.apache.hadoop/hadoop-core "0.20.2-dev"]
                                  ]}})
