(ns cube.source)

;; ## Utils functions to create/manipulate source

(defn guess-separator [lines]
  (let [separators (map str (seq "|;,:=+"))
        slines (map (fn [sep]
                      (map (fn [line] (.split line
                                              ({"|" "\\|"} sep sep)))
                           lines)
                      separators))]))

(defn guess-format
  "Find separator, type of fields , names if header present"
  [lines]
  )
