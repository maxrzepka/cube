(ns cube.logging
  "Some useful logging related stuff
TODO try to use clj-logging-config
"
  )

;;Coming from hack-wall
(defn field
    "Access to private or protected field. field-name must be something Named

   class - the class where the field is declared
   field-name - Named
   obj - the instance object, or a Class for static fields"
      [class field-name obj]
        (-> class (.getDeclaredField (name field-name))
                (doto (.setAccessible true))
                    (.get obj)))

;;coming from riemann.logging
(defn nice-syntax-error
  "Rewrites clojure.lang.LispReader$ReaderException to have error messages that
  might actually help someone."
  ([e] (nice-syntax-error e "(no file)"))
  ([e file]
   ; Lord help me.
   (let [line (field (class e) :line e)
         msg (.getMessage (or (.getCause e) e))]
    (RuntimeException. (str "Syntax error (" file ":" line ") " msg)))))
