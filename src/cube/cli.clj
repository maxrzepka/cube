(ns cube.cli
  (:require [cube.core :as c]
            [cube.config :as cf]
            [clojure.string :as s])
  (:use [clojure.tools.cli :only [cli]])
  (:gen-class))

(defn prompt-read [prompt]
  (print (format "%s> " prompt))
  (flush)
  (read-line))

(def definitions (atom #{}))

(defn find-definitions [code]
  (first
   (filter #(= code (:code %))
           @definitions)))

;; list of strings -> hash
;; by default key are converted into keyword
;; value converted into set with , space are separator
;;


(defn re-partition
  "Splits the string into a lazy sequence of substrings, alternating
  between substrings that match the patthern and the substrings
  between the matches.  The sequence always starts with the substring
  before the first match, or an empty string if the beginning of the
  string matches.

  For example: (re-partition #\"[a-z]+\" \"abc123def\")

  Returns: (\"\" \"abc\" \"123\" \"def\")"
  [^java.util.regex.Pattern re string]
  (let [m (re-matcher re string)]
    ((fn step [prevend]
       (lazy-seq
        (if (.find m)
          (cons (.subSequence string prevend (.start m))
                (cons (re-groups m)
                      (step (+ (.start m) (count (.group m))))))
          (when (< prevend (.length string))
            (list (.subSequence string prevend (.length string)))))))
     0)))

(defn infer
  "infers if string s represents a hash , a list or a plain string"
  [s]
  (cond
   (re-find #"^\s*\w+:" s)
   (reduce (fn [m [k v]]
             (assoc m (keyword (re-find #"\w+" k)) (infer v)))
           {}
           (partition 2 (next (re-partition #"\w+:" s))))
   (re-find #"," s)
   (mapv s/trim (s/split s #"\s*,\s*"))
   :else #{(keyword s)}))

;; convert sequence if strings into query map ie
;; query acct aggregate region filter "type: A , B "
;; becomes a clj structure
;; {:aggregate #{:region} :filter {:type ["A","B"]} :source acct}
(defn ->query
  [[source & args]]
  (let [query (reduce (fn [a [k v]]
                        (assoc a (keyword k) (infer v)))
                      {} (partition 2 args))]
    ;;query
    (assoc query :source (find-definitions source))
    ))

(def commands
  "List commands available from the console and their corresponding function returning "
  {:config cf/config
   :init (fn [& args]
           (let [dirs (if (seq args) args [(cf/config :def-dir)])]
             (reset! definitions (set (mapcat #(cf/load-files %) dirs)))))
   :def (fn [& args]
          (doseq [dir args]
            (swap! definitions into (cf/load-files dir))))
   :list (fn [& args]
           (map #(clojure.string/join " : " ((juxt :code :fields) %)) @definitions))
   :view (fn [& args] (c/page (find-definitions (first args))))
   :query (fn [& args] (c/execute (->query args)))
   :desc (fn [& args]
           (dissoc (find-definitions (first args)) :data))
   :help (constantly "Available commands :
  list : list all available sources
  desc <source name> : describe the source
  view <source name> : view content of the source
  query <source name> arguments : execute a query on the source for example
                      query acct aggregate region filter \"type: A\" ")
   })

(defn run
  "Runs a command-line and returns corresponding result as map"
  [line]
  (let [cmd (clojure.string/split line #" ")
        action (commands (keyword (first cmd))
                         (constantly {:error (str "command not found " cmd)}))]
    (apply action (rest cmd))))

(defn display [res]
  (cond
     (:error res)
     (println (str "ERROR " (:error res)))
     (map? res)
     (doseq [[k v] res] (println (str (name k) " = " (if (seq? v) (doall v) v))))
     (coll? res)
     (let [ res (doall res)]
       (if (coll? (first res))
               ;;TODO use [doric "0.7.0"] to accept list of maps/vectors
               (clojure.pprint/print-table res)
               (doseq [e res] (println e))))
     :else  (println res)
     ))

(defn console []
  ;;init
  (swap! definitions into (cf/load-files (cf/config :def-dir)))
  (loop [res []]
    ;;display result
    (display res)
    (let [line (prompt-read "cube")]
      (when-not (.startsWith line "quit")
        (recur (run line))))))

(defn -main [& args]
  (let [[opts args banner] (if (seq args) (apply cli args) (cli nil))]
    (when (seq args)
      (do (println args)
          (println "Executing....")
          (prn (c/execute (read-string (slurp (first args))))))
      )
    (console)))
