(ns cube.config
  (:require [cube.logging :as logging]
            [cube.io :as io]
            [carica.core :refer [configurer resources load-config]]))

(def config (configurer (resources "cube.clj")))

(defn default-data-dir []
  (System/getProperty "java.io.tmpdir"))

(defn add-source [source])

(defn add-db [db])

;;
;; functions copied from riemann/config.clj
;;
(defn read-strings
  "Returns a sequence of forms read from string."
  ([string]
     (read-strings []
                   (-> string (java.io.StringReader.)
                       (clojure.lang.LineNumberingPushbackReader.))))
  ([forms reader]
     (let [form (clojure.lang.LispReader/read reader false ::EOF false)]
       (if (= ::EOF form)
         forms
         (recur (conj forms form) reader)))))

(defn validate-config
  "Check that a config file has valid syntax."
  [file]
  (try
    (read-strings (slurp file))
    (catch clojure.lang.LispReader$ReaderException e
      (throw (logging/nice-syntax-error e file)))))

(defn include
  "Include another config file.

  (include \"foo.clj\")"
  [file]
  (binding [*ns* (find-ns 'cube.config)]
    (validate-config file)
    (load-string (slurp file))))

(defn load-files
  "Loads files in location returns a list of maps
"
  [location]
  (set
   (let [loc (clojure.java.io/file location)]
     (if (.isFile loc)
       [(load-config (.toURL loc))]
       (for [f (file-seq loc) :when (.isFile f)]
         (load-config (.toURL f)))))))