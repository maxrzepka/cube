(ns cube.web.server
  (:require [cube.core :as c]
            [cube.query :as q]
            [cube.io :as io]
            [cube.dataset :as d]
            [cube.scraping :as s])
  (:use [net.cgrand.moustache :only [app delegate]]
        [ring.middleware.params :only [wrap-params]]
        [ring.middleware.keyword-params :only [wrap-keyword-params]]
        [ring.middleware.reload :only [wrap-reload]]
        ;;        [ring.middleware.session :only [wrap-session]]
        [ring.middleware.file :only [wrap-file]]
        [ring.middleware.stacktrace :only [wrap-stacktrace]]
        [ring.util.response :only [response file-response redirect not-found]]
        [ring.middleware.format-response :only [wrap-json-response]]
        [ring.middleware.format-params :only [wrap-json-params]]
        [ring.adapter.jetty :only [run-jetty]]))

(defn render [t]
  (apply str t))

(def render-to-response
  (comp response render))

;; from https://github.com/swannodette/enlive-tutorial
(defn render-request [afn & args]
  (fn [req] (render-to-response (apply afn args))))

;;init : load in memory all datasets available
(def datasets
  {:acct (assoc (d/acct-dataset 20) :name "Accounts" :code "acct")
   :industry s/industry-source
   })


(def queries (atom {}))
(def sources (atom {}))

(def reports (atom {}))

(defn load-reports
  "Load into atom reports existing reports present under folder"
  [folder]
  )

(defn add-worker
  "A single thread execute in infinite loop any queries waiting in queue q"
  [q]
  (future
    (while true
      (let [m (.take q)
            id (c/compute-id m)]
        (when-not (@sources id)
          (try
            (let [r  (assoc (c/execute m) :code id)]
              (swap! sources assoc id r))
            (catch Throwable t
              (swap! queries update-in [id] assoc :error (str t))
              (.printStackTrace t))
            (finally
              (println (str "Attempted to process " id " : " (dissoc m :source))))))))))

(def queue
  (let [q (java.util.concurrent.LinkedBlockingDeque.)
        worker (add-worker q)]
    q))


(defn send-query [query]
  (let [id (c/compute-id query)]
    (when-not (@queries id)
      (.offer queue query)
      (swap! queries assoc id query))
    id))

(defn find-source [id]
  (datasets (keyword id) (get @sources id)))

(defn find-query [id]
  (when-let [q (get @queries id)]
    (assoc q :id id)))

(defn format-query
  [q]
  (when (map? q)
    (let [code (-> q :source :code)]
      (assoc q :source code))))

(defn query-description [q]
  ())

(defn query-infos [id]
  (when-let [q (find-query id)]
    ))

(defn create-report [q]
  (fn [{:keys [format fields]}])
  )

(defn query-result [id]
  (let [s (@sources id)]
    (c/page s)))

(defn aggregates [source]
  (when source
    (let [dims (:dimension source)
          aggregates (cons #{} (q/all-subsets dims))]
      (for [agg aggregates]
        {:fields (filter agg dims)
         :code (send-query {:aggregate agg :source source})
         :source (:code source)}))))

(defn dim-values [source dimension]
  (when-let [dimension (dimension (set (:dimension source)))]
    (let [query {:aggregate #{dimension} :source source}
          resultset (query-result (send-query query))]
      (when resultset
        {:code dimension
         :name (first (:header resultset))
         :values (mapv first (:rows resultset))}))))

(defn all-dim-values [source]
  (for [dim (:dimension source)]
    (dim-values source dim)))

(defn resp [data & [message]]
  (fn [req]
    (if data
        (response data)
        (not-found message))))

(defn add-query[{:keys [params] :as req}]
  (if-let [source (find-source (:source params))]
    (let [query (assoc params :source source)
          id (send-query query)]
      ((resp (-> id find-query format-query) (str "Query " id " not created")) req))
    (not-found (str "Source not found " (:source params)))))

(defn get-report [id]
  (when-let [location (get @reports id)]
    (file-response location)))

(defn report-infos [q m]
  (let [{:keys [fields format] :or {format "txt"}} m
        r (assoc q :fields fields :format format)
        s (get @sources (:id q))
        id (c/compute-id r)
        filepath (str "reports/"
                      (:code (:source q) (:source q))
                      "_" id "." format)]
    {:source s :filepath filepath :id id :format format :link (str "report/" id)}))

(defn create-report
  "Generate report if not existing"
  [q]
  (fn [{params :params}]
    (let [infos (report-infos q params)
          id (:id infos)]
      (when-not (get @reports id )
        (do (io/export-source (:source infos) (dissoc infos :source))
            (swap! reports assoc (:id infos) (:filepath infos))))
      infos)))

;;First cut : json accept/content-type
(def routes
  (app
   (wrap-file "resources")
   (wrap-stacktrace)
   ;;   (wrap-reload)
   ;;   (wrap-session)
   (wrap-json-response)
   (wrap-json-params)
   (wrap-params)
   (wrap-keyword-params)
   ["source"]
   {:get (resp (vals datasets))
    :post add-query}
   ["source" id] (resp (find-source id) (str "Source [" id "] not found" ))
   ["source" id "aggregates"] (resp (aggregates (find-source id)))
   ;;TODO handle also file based data source
   ["source" id "data"] (resp (:data ((keyword id) datasets)))
   ["source" id "values"] (resp (all-dim-values (find-source id)))
   ["source" id "values" dim] (resp (dim-values (find-source id) (keyword dim)))
   ["query"] {:post add-query}
   ["query" id] (resp (-> id find-query format-query) (str "Query [" id "] not found"))
   ["query" id "data"] (resp (query-result id) (str "Query [" id "] not found"))
   ["query" id "export"] (create-report (find-query id))
   ["report" id] (resp (get-report id) (str "Report [" id "] not found"))
   ))

;;TODO load some config : data sources , report repository
(defn start [ & [port & options]]
  (run-jetty (var routes) {:port (or port 8080) :join? false}))

(defn -main []
  (start))
