(ns cube.core
  (:use [cube.cascalog :only [cascalog-execute]]
        [cube.sql :only [sql-execute]])
  (:require [cube.io :as io]))


;; ##  Query Functions
;;
;; Query can aggregate and/or filter datas
;; Data is a list of rows where each field is either :
;;
;; 1. Dimension only them can be aggregated
;; 2. Measure can be sum up in aggregation
;; 3. Key : counting in aggregation
;;
;; Queries and data are defined in a single plain map,
;; data definition is appended to the query map
;;
;; 1. Query operation : :aggregate :filter , empty hash leaves data as it is
;; 2. Data definition (source) : :dimension :key :measure
;;
;;
;; Note : map values can be a collection or a keyword .
;; ? map values should always be of same type ?

(defn valid-query?
  "aggregate if present is a set"
  [{:keys [aggregate dimension key measure]}]
  (or (nil? aggregate) (set? aggregate)))

(defn agg-query? [q]
  (boolean (:aggregate q)))

(defn val->coll [v]
  (if (sequential? v)
    v
    [v]))

(defn compute-id [query]
  (.replace (Integer/toString (hash query) 36) "-" "a"))

(defn add-dimension
  "Add a dimension to a source :
(add-dimension (generate-dataset 20) :bbalance [:bucket :balance 0 100 1000 10000]) "
  [spec name desc]
  (-> spec
      (update-in [:dimension] conj name)
      (update-in [:custom] (fnil conj {}) [name desc])))

;;only useful for aggregate queries
(defn fields [src]
  (vec (concat (:fields src) (keys (:custom src)))))

;;TODO add join syntax [:join source1 source2] , [[:join :left] source1 source2]
(defn desc
  "Describes a source : returns map of dimension, measure key"
  [source]
  (let [ks [:fields :dimension :measure :key :custom]
        sources (if (map? source) [(select-keys source ks)]
                    (map #(select-keys % ks) (rest source)))
        ]
    (apply merge-with (comp vec distinct concat) sources)))

(defn kw->title
  "Converts keyword to a string for display"
  [k]
  (-> k name clojure.string/capitalize))

(defn q->fields
  "Returns fields of the returning data ie header
In specific order : dimensions , key and measures "
  [{:keys [aggregate source] :as q}]
  (let [{:keys [key dimension measure] :as s} (desc source)]
    (if aggregate
            (vec
             (concat (keep (fn [a] (when (aggregate a) (kw->title a))) dimension)
                     key
                     measure))
            (fields s))))

(defn q->header
  "Returns headers of result set of query"
  [{:keys [aggregate source] :as q}]
  (let [{:keys [key dimension measure] :as s} (desc source)]
  (if aggregate
    (vec
     (concat (keep (fn [a] (when (aggregate a) (kw->title a))) dimension)
             (mapv #(str (kw->title %) " #") key)
             (mapv kw->title measure)))
    (mapv kw->title (fields s)))))

(defn source->header [{:keys [fields titles]}]
  (mapv #(% titles (kw->title %)) fields))

(defn source->type [{:keys [fields] :as s}]
  (let [types (into {}
                    (mapcat (fn [[k coll]] (map #(vector % k) coll))
                            (select-keys s [:dimension :measure :key])))]
    (map types fields)))

;; ## Expansion Functions

(defn query-orders
  "Defines order instruction from query"
  [{:keys [aggregate dimension] :as q}]
  (when-let [sort (seq
                   (when (seq aggregate)
                     (filter aggregate dimension)))]
    (vector (vec (concat [:sort] sort)))))

;;TODO add select operation : skip aggregation predicates if not in select
;;TODO improve it : awkward specs
(defn dimensions
  "Returns dimensions of the query"
  [{:keys [select fields aggregate dimension custom]}]
  (if aggregate
    (if (seq aggregate)
      (filterv aggregate dimension)
      [])
    (if select
      select ;; (filter (set select) raw) => returns raw !!!!
      (concat dimension (keys custom)))))

(defn predicates
  [{:keys [aggregate dimension filter key measure custom]}]
  (let [filters (keep (fn [[k v]]
                        (when v
                          (if (sequential? v)
                            [:in k v]
                            [:= k v])))
                      filter)
        aggregates (when aggregate
                     (concat
                      (map #(vector :count %) key)
                      (map #(vector :sum %) measure)))
        custom (map (fn [[k d]] (vec (list* (first d) k (rest d)))) custom)]
    {:aggregate aggregates :filter filters :custom custom}))

(defn orders
  [{:keys [aggregate dimension]}]
  (when-let [sort (seq
                   (when (seq aggregate)
                     (filter aggregate dimension)))]
    (vector (vec (concat [:sort] sort)))))

(defn expand
  "Expands query into predicate dimension measure"
  [query]
  (let [{:keys [select aggregate filter source ] :as q1} query
        {:keys [fields dimension measure key custom] :as q2} (desc source)
        q (merge q1 q2)
        raw (concat fields (keys custom))
        sums (vec (concat key measure))
        meas sums;; (if aggregate sums [])
        preds (remove nil? (apply concat (vals (predicates q))))
        dims (dimensions q)
        fields (when (nil? aggregate) fields)]
    {:predicate preds :dimension dims :fields fields :measure meas :source source}))

(defn source->type [source]
  (cond
   (:connection source) :db
   (:filepath source) :file
   (:data source) :memory
   :else nil
   ))

(defn query->access
  "Returns access informations as maps,
on error returns "
  ([query] (query->access query :file))
  ([query src & options]
     (let [options (apply hash-map options)]
       (condp = src
         :file (let [filepath (-> query :source :filepath)
                       folder (if-let [p filepath]
                                (.getPath (.getParentFile (java.io.File. p)))
                                (System/getProperty "java.io.tmpdir"))
                       folder (:repository options folder)
                       newfilepath (str folder "/" (compute-id query))]
                   {:filepath newfilepath :separator "|"})
         :db {:host :todo}
         :memory {:data nil}
         {:error  (str "Source type unknown " src)}))))

;; TODO find best strategy to get
;;

(defn find-src-type
  "Find output source type given a query"
  [query & [out]]
  (let [source-type (-> query :source source->type)]
    (cond
     out out
     (#{:db :file} source-type) :file
     :else source-type)))

(defn query->source
  "Returns source after query execution
- by default : in-memory / file is preserved can be changed through options
- file location : use by default the parent folder of the query source
"
  [query & options]
  (let [source (:source query)
        {:keys [filepath key]} source
        {:keys [dimension measure]} (expand query)
        ;;TODO change some measure field : key -> key # ???
        ;;By default output source same type as input source
        src-type (find-src-type query (:output (apply hash-map options)))
        access (query->access query src-type)
        #_(apply query->access (list* query options))]
    (merge {:dimension dimension
            :measure measure
            :fields (concat dimension measure)
            :key (when-not (seq measure) key);;key stays when no aggregation
            }
           access)))

;; ## Execution Functions

(defn find-logic [query]
  (when-let [source (:source query)]
    (condp = (source->type source)
      :db sql-execute
      cascalog-execute)))

(defn execute
  "Execute a query and returns the corresponding source"
  [query & options]
  (let [source (query->source query)
        execute* (find-logic query)
        data (execute* (expand query) source)]
    (merge source data)))

;;TODO handle pagination
(defn page
  ([source]
     (page source 1 nil))
  ([source numero]
     (page source numero nil))
  ([source numero size]
     (when source
       {:header (source->header source)
        :field (:fields source)
        :type (source->type source)
        :rows (if-let [data (:data source)]
                data
                (io/file->rows source))})))
