(ns cube.cascalog
  (:require [cascalog.api :as ca]
            [cascalog.ops :as co]
            [cascalog.tap :as tap]
            [cascalog.vars :as v]
            [cascalog.workflow :as w])
  (:import [cascading.scheme.local TextDelimited]
           ;;[cascading.scheme.hadoop TextDelimited WritableSequenceFile]
           [cascading.tuple Fields]))

;; FS delimited taps

(defn- delimited
  [field-seq delim & {:keys [classes skip-header? quote]}]
  (let [skip-header? (boolean skip-header?)
        quote (or quote "\"")
        field-seq    (w/fields field-seq)
        field-seq    (if (and classes (not (.isDefined field-seq)))
                       (w/fields (v/gen-nullable-vars (count classes)))
                       field-seq)]
    (if classes
      (TextDelimited. field-seq skip-header? delim quote (into-array classes))
      (TextDelimited. field-seq skip-header? delim quote))))

(defn hfs-delimited
  "Creates a tap on HDFS using Cascading's TextDelimited
   scheme. Different filesystems can be selected by using different
   prefixes for `path`.

  Supports keyword option for `:outfields`, `:classes` and
  `:skip-header?`. See `cascalog.tap/hfs-tap` for more keyword
  arguments.

   See http://www.cascading.org/javadoc/cascading/tap/Hfs.html and
   http://www.cascading.org/javadoc/cascading/scheme/TextDelimited.html"
  [path & opts]
  (let [{:keys [outfields delimiter]} (apply array-map opts)
        scheme (apply delimited
                      (or outfields Fields/ALL)
                      (or delimiter "\t")
                      opts)]
    (apply tap/hfs-tap scheme path opts)))

(defn lfs-delimited
  "Creates a tap on the local filesystem using Cascading's
   TextDelimited scheme. Different filesystems can be selected by
   using different prefixes for `path`.

  Supports keyword option for `:outfields`, `:classes` and
  `:skip-header?`. See `cascalog.tap/hfs-tap` for more keyword
  arguments.

   See http://www.cascading.org/javadoc/cascading/tap/Hfs.html and
   http://www.cascading.org/javadoc/cascading/scheme/TextDelimited.html"
  [path & opts]
  (let [{:keys [outfields delimiter]} (apply array-map opts)
        scheme (apply delimited
                      (or outfields Fields/ALL)
                      (or delimiter "\t")
                      opts)]
        (apply tap/lfs-tap scheme path opts)))

;; ## Cascalog-specific Functions

(defn keyword->casvar [k & [prefix]]
  (str "?" prefix (name k)))

(defn data->generator
  "converts list of hash-maps to a cascalog generator (array-of-arrays vars)"
  [{:keys [data fields]}]
  (let [maps (map? (first data))
        fields (if (and maps (not (seq fields)))
                 (keys (first data))
                 fields)
        rows (mapv (if maps (apply juxt fields) identity) data)]
    (list* rows (map #(str "?" (name %)) fields))))

(defn file->generator
  [{:keys [separator filepath fields dimension measure] :or {separator "|"}}]
  (let [m (set measure)
        classes (map (fn [f] (if (m f) Float String)) fields)
        vars (map #(str "?" (name %)) fields)]
    (list* (lfs-delimited filepath
                        :delimiter separator
                        :outfields vars
                        :classes classes
                        :skip-header? true)
         vars)))

;; ?? handle logic for join : outer join need nullable variable
(defn source->generator
  "Returns corresponding cascalog generator (in-memory , file tap)"
  [source]
  (cond (:data source) ;;one source
        (data->generator source)
        (:filepath source)
        (file->generator source)
        :else source))

(defn multi-sources [source]
  (if (map? source)
    (vector (source->generator source))
    (map source->generator (rest source))))

(defn interval->str [coll]
  (apply str (next (interleave (repeat " - ") coll))))

(defn plain-bucketize
  "Return bucket of value, required buckets to be sorted"
  [buckets value]
  (cond
    (> (first buckets) value) (str "- " (first buckets))
    (<= (last buckets) value) (str (last buckets) " -")
    :else (->> buckets
               (partition 2 1)
               (filter (fn [[low up]]
                         (or (not up)
                             (and
                              (<= low value)
                              (< value up)))))
               first
               interval->str)))

;; parametrized operator :
;; (ca/??<- ["?tt"] ([[23] [80] [99]] "?t") (bucketize [[0 30 50 100]] "?t" :> "?tt"))
;; (["0 - 30"] ["50 - 100"] ["50 - 100"])
(ca/defmapop [bucketize [buckets]] [value]
  (plain-bucketize buckets value))

(defn in [field values]
  (some (fn [v] (= field v)) values))

;; TODO both below functions use "t" prefix, how to make it cleaner
(defn cascalog-predicate [pred]
  (let [[op name & args] pred]
    (condp = op
      :count [co/!count (keyword->casvar name) :> (keyword->casvar name "t")]
      :sum [co/sum (keyword->casvar name) :> (keyword->casvar name "t")]
      := [#'= (keyword->casvar name) (first args)]
      :in [#'in (keyword->casvar name) (first args)]
      :bucket [bucketize (vector (vec (rest args)))
               (keyword->casvar (first args)) :> (keyword->casvar name)]
      (vec (cons op (map keyword->casvar (rest pred)))))))

;;
(defn cascalog-outvars
  "Handle raw case and mix of dimension and aggregate columns"
  ([unchanged] (cascalog-outvars unchanged []))
  ([unchanged aggregated]
      (vec (concat
            (map keyword->casvar unchanged)
            (map #(keyword->casvar % "t") aggregated)))))

(defn cascalog-query
  "convert expanded query to cascalog query"
  [query]
  (let [{:keys [predicate fields measure dimension]} query
        generator (multi-sources (:source query))
        predicates (vec (concat generator (mapv cascalog-predicate predicate)))
        outvars (some seq [(cascalog-outvars fields)
                           (cascalog-outvars dimension measure)])]
    (ca/construct outvars predicates)))

(defn cascalog-execute
  "Convert expanded query to cascalog query and execute it
catch any error and return error map"
  [query & [{:keys [filepath]}]]
  (let [q (cascalog-query query)]
    (try
      (if filepath
       (when-not (.exists (java.io.File. filepath))
         (ca/?- (lfs-delimited filepath :delimiter "|") q))
       {:data (first (ca/??- q))})
      (catch Exception e
        ;;TODO store more error infos stacktrace
        {:error (str "cascalog-execute failed : " (.getMessage e))}))))
