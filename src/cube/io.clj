(ns cube.io
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]))

(defn source->files
  "Get all files of a source (exclude crc files) "
  [s]
  (when-let [filepath (:filepath s)]
    (let [file (io/file filepath)
          ]
      (if (.isFile file)
        [filepath]
        (keep #(when (not (.endsWith % ".crc"))
                 (str (.getAbsolutePath file) "/" %))
              (.list file))))))

(defn to-char
  "Returns a character if possible : char or first character of string"
  [s]
  (cond (char? s) s
        (string? s) (first (seq s))
        :else nil))

(defn write-csv [filepath separator data]
  {:pre [(every? (comp not nil?) [filepath separator data])]}
  (with-open [wrt (io/writer filepath)]
    (csv/write-csv wrt data :separator (to-char separator))))

(defn files
  "returns list of absolute paths, on directory returns files inside excluding crc "
  [filepath]
  (let [file (io/file filepath)
          files (if (.isFile file)
                  [filepath]
                  (keep #(when (not (.endsWith % ".crc"))
                           (str (.getAbsolutePath file) "/" %))
                        (.list file)))]
      files))

;; TODO implement batch extraction
(defn file->rows
  "Returns rows from file "
  ([access] (file->rows access 0 20))
  ([access start] (file->rows access 0 (+ start 20)))
  ([{:keys [filepath separator]} start end]
     (when filepath
       (apply concat
              (for [f (files filepath)]
                (with-open [rdr (io/reader f)]
                  (doall (csv/read-csv rdr :separator (to-char separator)))))))))

(defn export-source
  [source {:keys [filepath format]}]
  (let [output (java.io.File. filepath)]
    (when (not (.exists output))
      (with-open [wtr (io/writer filepath)]
        (if (:data source)
          (doseq [row (:data source)]
             (.write wtr (str (clojure.string/join ";" row) "\n")))
          (let [delimiter (:separator source)]
           (doseq [f (source->files source)]
             (with-open [rdr (io/reader f)]
               (csv/write-csv wtr (csv/read-csv rdr :separator (to-char delimiter)) :separator ";")))))))))
