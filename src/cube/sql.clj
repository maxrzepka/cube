(ns cube.sql
  (:require [clojure.string :as s]
            [cube.io :as io]
            [clojure.java.jdbc :as db]))

(defn inquote [s]
  (str "'" s "'"))

(defn predicate->sql
  ([pred] (predicate->sql "" pred))
  ([alias [op field & args]]
     (let [label (name field)
           field (str (when (seq alias) (str alias ".")) (name field))]
       (condp = op
         :count (str "count(distinct " field ") " label)
         :sum   (str "sum(" field ") " label)
         :in (str field " in (" (s/join (map inquote (first args)) ",") ") ")
         := (str field " = " (inquote (first args)))
         nil))))

;; TODO how to join tables
(defn query->sql
  "Convert query to a SQL statement. Query's source should reside in DB."
  [query]
  (if-let [table (-> query :source :table)]
    (let [{:keys [predicate dimension measure]} query
          filter-ops  #{:in :=}
          filters (filter (comp filter-ops first) predicate)
          aggs (filter (comp not filter-ops first) predicate)]
      (str "select " (s/join ", " (concat (map name dimension)
                                          (map predicate->sql aggs)))
           " from " (name table)
           ;;where clause
           (when (seq filters)
             (str " where " (s/join " and " (map predicate->sql filters))))
           ;;group by clause
           (when (seq dimension)
             (str " group by " (apply str (s/join ", " (map name dimension)))))
           ))))

;; TODO handle when output is in-memory or in DB

(defn result-write [rows source]
  )

(defn sql-execute
  "Run SQL query "
  [query source]
  (let [conn (-> query :source :connection)
        sql (query->sql query)
        separator (:separator source)]
    (db/with-connection conn
      (db/with-query-results rows [sql]
        (io/write-csv (:filepath source) separator
                      (map (apply juxt (:fields source)) rows))))))
