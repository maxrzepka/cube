(ns cube.query
  (:require [cube.core :as c]))

(defn toggle-aggregate
  "Toggle a aggregate of the query and returns the new query"
  [{:keys [aggregate] :as q} agg]
  {:post [(c/valid-query? %)]}
  (update-in q [:aggregate]
               (fnil (if (agg aggregate) disj conj) #{})
               agg))

(defn details-query
  "returns filter query corresponding of row of an aggregate query"
  [{:keys [aggregate dimension] :as q} row]
  (if aggregate
    (assoc (dissoc q :aggregate)
      :filter (when (seq aggregate)
                (zipmap (filter aggregate dimension) row)))))

(defn subsets
  [n coll]
  (cond
   (< n 1) (list #{})
   (empty? coll) #{}
   :else (concat (map
                  #(conj % (first coll))
                  (subsets (dec n) (rest coll)))
                 (subsets n (rest coll)))))

(defn all-subsets [coll]
  (let [coll (distinct coll)]
    (mapcat #(subsets % coll) (range 1 (inc (count coll))))))

;; TODO extend when data is not in-memory : fetch all distinct values for each dimension
(defn filter-values
  "Returns a map : combination of all dimensions => all values as a map found in data
For example [:type] => #{ {:type \"A\"} {:type \"B\"} }
"
  [{:keys [dimension data]}]
  (let [ks (all-subsets dimension)
        init-map (reduce (fn [m k] (assoc m k #{})) {} ks)]
    (->>
     (reduce (fn [m r]
               (reduce (fn [m1 k] (update-in m1 [k] conj (select-keys r k)))
                       m ks))
             init-map
             data)
     (remove #(nil? (seq (second %))))
     (into {}))))


(defn agg-queries
  "Get all aggregate queries and their details queries
"
  [spec]
  (let [all (filter-values spec)]
    (concat
     (map #(assoc {} :aggregate (set %)) (cons nil (keys all)))
     (map #(assoc {} :filter %) (cons nil (mapcat second all))))))

;; ## Utils Functions

(defn load-data
  "Returns data with all custom fields computed"
  [spec]
  (let [fields (c/q->fields spec)
        rows (:rows (c/execute spec))]
    (map #(zipmap fields %) rows)))

(defn load-facets
  "Execute all aggregate and filters queries from a data set.
"
  [spec]
  (let [spec (if (:custom spec) (assoc spec :data (load-data spec)) spec)]
    (map #(c/execute (merge % spec))
               (agg-queries spec))))

(defn extract-header
  [^String line & {:keys [separator comment] :or {separator "\\|" comment "#"}}]
  (when (and line (.startsWith line comment))
    (map #(-> % (clojure.string/replace #"[\W_]+" "-") (.toLowerCase ) keyword)
         (seq (.split (.substring line 1) separator)))))

(defn as-double [s]
  (try
    (Double/parseDouble s)
    (catch Exception e -1)))

(defn file->data
  "Load file in memory "
  [file & {:keys [separator measure] :or {separator "\\|"}}]
  (with-open [rdr (java.io.BufferedReader.
                   (java.io.FileReader. file))]
    (let [lines (line-seq rdr)
          header (extract-header (first lines) :separator separator :comment "#")
          lines (if (seq header) (rest lines) lines)
          ks (if (seq header) header (map keyword (iterate inc 1)))
          convert-fns (map #(if ((set measure) %) as-double identity) ks)]
      {:fields ks
       :data (mapv (fn [l]
                     (zipmap ks
                             (map #(%1 %2) convert-fns (.split l separator))))
                   lines)})))

(defn file->spec [& {:keys [file separator dimension key measure]
                     :or {separator "\\|"} :as spec}]
  (merge spec (file->data file :separator separator :measure measure)))

(defn as-int [s]
  (try
    (Integer/parseInt s)
    (catch Exception e -1)
    ))

(defn file->cube [& {:keys [file separator dimension key measure]
                     :or {separator "\\|"} :as spec}]
  (let [spec (merge spec (file->data file :separator separator :measure measure))]
    (load-facets spec)))
