(ns cube.sql-test
  (:use [clojure.test])
  (:require [cube.sql :as s]
            [cube.core :as c]
            [cube.io :as io]
            [cube.dataset :as d]
            [clojure.java.jdbc :as db]))

#_(def mydb {:classname "com.mysql.jdbc.Driver"
                  :subprotocol "mysql"
                  :subname "//localhost:3306/test"
                  :user "test"
                  :password "test"})

(def mydb {:subprotocol "hsqldb"
           :subname "cube_test_hsqldb"})

(defn init-db []
  (db/with-connection mydb
     (db/do-commands
      "drop table if exists acct"
      (db/create-table-ddl
       :acct [:acct "varchar(10) primary key"] [:type "varchar(4)"] [:region "varchar(50)"]
       [:country "varchar(50)"] [:balance "double"] [:commission "double"] ))))

(defn populate-db [data]
  (db/with-connection mydb
    (db/delete-rows :acct ["1 = ?" 1])
    (apply db/insert-records :acct data)))

(use-fixtures
  :once
  (fn [test-runner]
   (init-db)
   (test-runner)))

#_(use-fixtures
  :each
  (fn [test-runner]
    ))

(deftest test-build-sql
  (testing "Testing predicate->sql"
    (is (= (s/predicate->sql [:count :acct]) "count(distinct acct) acct")
        (= (s/predicate->sql "a" [:count :acct]) "count(distinct a.acct) acct")))
  (testing "Testing query->sql"
    (let [q {:predicate (list [:count :acct] [:sum :balance] [:sum :commission])
             :dimension [:region]
             :measure [:acct :balance :commission]
             :source {:table :acct}}]
      (is (= (s/query->sql q) "select region, count(distinct acct) acct, sum(balance) balance, sum(commission) commission from acct group by region")))))

(defn acct-source []
  (-> (d/acct-dataset 10)
      (dissoc :data)
      (assoc :connection mydb :table :acct)))



(deftest test-execute-sql
  (testing "Testing execute SQL"
    (let [records (:data (d/acct-dataset 10))
          ;;res (init-db)
          res (populate-db records)
          query {:aggregate #{:region} :source (acct-source)}
          new-source (c/query->source query :repository "/tmp")
          res (s/sql-execute (c/expand query) new-source)
          data (io/file->rows new-source)]
      (do (prn new-source))
      (is (> (count data) 0))
      (is (every? #(= 4 (count %)) data))
      (is (distinct? (map first data))))))

(deftest test-execute
  (testing "Testing execute"
    (let [res (populate-db (:data (d/acct-dataset 10)))
          query {:aggregate #{:region} :source (acct-source)}
          new-source (c/execute query)
          data (io/file->rows new-source)]
      (do (prn new-source))
      (is (> (count data) 0))
      (is (every? #(= 4 (count %)) data))
      (is (distinct? (map first data))))))
