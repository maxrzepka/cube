(ns cube.io-test
  (:use [clojure.test])
  (:require [cube.io :as i]
            [clojure.java.io :as io]))

(deftest test-file->rows
  (testing "get rows"
    (let [filepath "data/test.csv"
          rows (i/file->rows {:filepath filepath :separator ","})
          file (io/file filepath)]
      (is (.exists file))
      (is (= 10 (count rows)))
      (is (every? #(= 3 (count %)) rows)))))

(deftest test-csv
  (testing "write csv file from lazy sequence"
    (let [d (lazy-seq (map vector (range 1000)))
          tempfile (.getAbsolutePath
                    (io/file (System/getProperty "java.io.tmpdir") "test1.csv"))
          res (i/write-csv tempfile "," d )]
      (do (println (str "wrote " tempfile)))
      (is (= 1000 (count (line-seq (io/reader tempfile))))))))