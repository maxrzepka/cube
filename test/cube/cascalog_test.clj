(ns cube.cascalog-test
  (:use clojure.test)
  (:require [cube.cascalog :as c]
            [cube.logging :as l]
            [cascalog.api :as api]))

(use-fixtures
  :once
  (fn [test-runner]
    (test-runner)
   ;;(l/with-log-level :info (test-runner))
    ))

(deftest test-data->generator
  (testing "test list of hashs"
    (is (= (list [["A"] ["B"] [ "C"]] "?a")
           (c/data->generator {:data [{:a "A" :b 2} {:a "B" :b 4} {:a "C" :b 7}] :fields [:a]})))
    )
  (testing "list of arrays"
    (is (= (list [["A"] ["B"] [ "C"]] "?a")
           (c/data->generator {:data [["A"] ["B"] ["C"]] :fields [:a]})))))

(def csv-source
  {:name "Gender Heigth/Weight"
   :code "genderhw"
   :filepath "data/test.csv"
   ;; :filepath "data/01_heights_weights_genders.csv"
   :separator ","
   :fields [:gender :height :weight]
   :measure [:height :weight]
   :dimension [:gender]
   })

(deftest test-csv-source
  (testing "test lfs-delimited"
    (let [data (api/??<- ["?gender" "?height" "?weight"]
                         ((c/lfs-delimited
                           "data/test.csv" :delimiter ","
                           :outfields ["?gender" "?height" "?weight"])
                          "?gender" "?height" "?weight"))]
      (is (= (count data) 10))
      (is (= ["Gender","Height","Weight"] (first data)))))
  (testing "test file->generator"
    (let [data (api/??-
                (api/construct ["?gender" "?height" "?weight"]
                               (list (c/file->generator csv-source))))]
      (is (= 9 (count (first data))))))
  (testing "test execution without query ops"
    (let [q {:fields [:gender :height :weight]
             :dimension [:gender] :measure [:height :weight]
             :source csv-source}
          data (:data (c/cascalog-execute q))]
      (is (= (count data) 9))
      (is (= 3 (count (first data))))
      (is (every? (comp #{"Male" "Female"} first) data)))))
