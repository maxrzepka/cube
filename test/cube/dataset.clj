(ns cube.dataset)

;; ## Generate Random Dataset

(defn acct-dataset
  "Generate dataset based on types , countries"
  [size]
  (let [types ["A" "B" "C" "D"]
        countries {"France" "Europe"
                   "UK" "Europe"
                   "Germany" "Europe"
                   "USA" "America"
                   "Canada" "America"
                   "China" "Asia"
                   "India" "Asia"}]
    {:dimension [:type :region :country]
     :key [:acct]
     :measure [:balance :commission]
     :fields [:acct :type :region :country :balance :commission]
     :data (map (fn [i]
                ((fn [m] (assoc m :region (countries (:country m))))
                 {:type (get types (rand-int (count types)))
                  :country (-> (seq countries)
                               (nth (rand-int (count countries)))
                               first)
                  :acct (str "A" i)
                  :balance (rand-int 10000)
                  :commission (* 0.01 (rand-int 1000))
                  }))
              (range 1 (inc size)))}))

;;Generate 2 datasets
(defn name-datasets []
  (let [gender [ ["Célimène" "F"] ["Argante" "F"] ["Oraste" "M"] ["Alceste" "M"]
                 ["Philistin" "M"]
                 ]
        locations ["Paris" "London" "Berlin"]
        ]
    [{:fields [:name :gender :age]
      :key [:name]
      :data (map #(assoc (zipmap [:name :gender] %) :age (+ 20 (rand-int 30))) gender)}
     {:fields [:name :location]
      :key [:name]
      :data (map #(assoc {:name (first %)}
                    :location (get locations (rand-int (count locations))))
                 gender)}]))
