(ns cube.scraping
  (use [ring.util.codec :only [url-encode]]
       )
  (require [net.cgrand.enlive-html :as h]
           [clojure.java.io :as io]))

(defn yahoo-components [symbol]
  (let [url (str "http://finance.yahoo.com/q/cp?s=" (url-encode symbol))
        page (h/html-resource (java.net.URL. url))]
    (map (fn [s n]  {:symbol (h/text s) :name (h/text n)})
         (h/select page [:td.yfnc_tabledata1 :a])
         (h/select page [:table.yfnc_tableout1 :tr
                         [:td.yfnc_tabledata1 (h/nth-child 2)]]))))

(defn date->map [date]
  (let [strdate (.replaceAll (str date) "-" "")
        year (if (> (count strdate) 3) (Integer. (.substring strdate 0 4)) 0)
        month (if (> (count strdate) 5) (Integer. (.substring strdate 4 6)) 1)
        day (if (> (count strdate) 7) (Integer. (.substring strdate 6 8)) 1)]
    {:month month :year year :day day}))

(defn wget [url]
  (try (slurp url) (catch Exception e (println (str "skip " symbol)))))

(defn normalize [s]
  (.replaceAll s "\n" " "))

(defn s->filename [s]
  (.replaceAll s " " "_"))

(defn coll->line [sep coll]
  (str (normalize (apply str (rest (interleave (repeat sep) coll)))) "\n"))

(defn  yahoo-details [symbols]
  (let [parameters {:s "Symbol" :n "Name" :x "Exchange"
                    :c4 "Currency" :j1 "Market Capitalization"}
        order [:s :n :c4 :x :j1]
        params1 (apply str (map name order))
        titles (map parameters order)
        url (str "http://finance.yahoo.com/d/quotes.csv?s="
                 (apply str (rest (interleave (repeat "+") (map url-encode symbols))))
                 "&f=" params1)]
    ;url
    (wget url)
    ))

(defn yahoo-history
  "Get historical stats :
Date,Open,High,Low,Close,Volume,Adj Close
2012-08-01,3298.70,3328.50,3296.62,3321.56,133694800,3321.56
"
  [symbol start end]
  (let [start (date->map start)
        end (date->map end)
        url  (str "http://ichart.finance.yahoo.com/table.csv?ignore=.csv&g=d&s="
                  (url-encode symbol)
                  "&a=" (dec (:month start))
                  "&b=" (:day start)
                  "&c=" (:year start)
                  "&d=" (dec (:month end))
                  "&e=" (:day end)
                  "&f=" (:year end)
                  )]
    (wget url)))

(defn yahoo-scraper [url]
  (let [fullify (fn [page] (str "http://biz.yahoo.com/p/" page))
        extractor (fn [node] {:link (-> node :attrs :href fullify)
                              :name (h/text node)
                              :code (-> node :attrs :href)})]
    (map extractor
                     (h/select
                      (h/html-resource (java.net.URL. url))
                      [:td [:a (h/attr-ends :href "mktd.html")]]))))

(defn yahoo-symbols [{:keys [link name sector]}]
  (let [symbols (map h/text
                     (h/select (h/html-resource (java.net.URL. link))
                               [:td [:a (h/nth-child 2)]]))]
    (for [s symbols :when (not (or (= s sector) (.startsWith s "symbol")))]
      {:symbol s :industry name :sector sector})))


(defn yahoo-industries
  "List of industries for each sectors"
  []
  (let [url (fn [page] (str "http://biz.yahoo.com/p/" page))
        sectors (yahoo-scraper (url "s_mktd.html"))]
  ;  sectors
    (filter  #(not= "Sectors" (:name %))
            (mapcat (fn [s] (map (fn [i] (assoc i :sector (:name s)))
                                 (yahoo-scraper (:link s))))
                     sectors))))

(defn spit-companies []
  (with-open [wrt (io/writer "data/allcompanies.lst")]
    (.write wrt "#Symbol|Industry|Sector\n")
    (doseq [i (yahoo-industries)
            s (yahoo-symbols i)]
      (.write wrt (coll->line "|" (map s [:symbol :industry :sector]))))))

(defn spit-details [file & {:keys [sep pos] :or {sep "\\|" pos 0}}]
  (with-open [wrt (io/writer (str file ".dtls"))
              rdr (io/reader file)]
    (doseq [ids (partition 100 (map #(get (.split % sep) pos) (next (line-seq rdr))))]
      (.write wrt (yahoo-details ids)))))

(def industry-source
  {:name "Industries"
   :code "industry"
   :filepath "data/allcompanies.lst"
   :separator "|"
   :fields [:symbol :industry :sector]
   :dimension [:sector :industry]
   :key [:symbol]})

;;(def i (mapcat yahoo-scraper (map #(str "http://biz.yahoo.com/p/" % "mktd.html") (range 1 10))))
