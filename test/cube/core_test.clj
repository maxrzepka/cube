(ns cube.core-test
  (:require [cube.core :as c]
            [cube.dataset :as d]
            [cube.scraping :as s]
            [cube.logging :as l])
  (:use clojure.test))

#_(use-fixture)

(use-fixtures
  :once
  (fn [test-runner]
    (test-runner)
   ;;(l/with-log-level :warn (test-runner))
    ))

(defn dump-data [f data]
  (binding [*out* (java.io.FileWriter. f)]
    (prn data)))

(deftest test-dataset
  (testing "Test Dataset Generation"
    (let [d (d/acct-dataset 10)]
      (is (= 10 (count (:data d))))
      ))
  (testing "Query Execution"
    (let [d (d/acct-dataset 5)
          e (c/execute (assoc {:aggregate #{:region}} :source d))]
      (is (< 0 (count (:data e)))))))

(deftest test-empty-query
  (testing "Expand empty query"
    (let [source (d/acct-dataset 10)
          e (c/expand {:source source})]
      (is (= (set (:fields source))
             (set (apply concat ((juxt :dimension :measure) e))))))
    )
  (testing "Execute empty query"
    (let [source (d/acct-dataset 10)
          e (c/execute {:source source})]
      (is (= (:data e) (map (apply juxt (:fields source)) (:data source)))))))

(deftest test-custom-dimension
  (testing "Test Add custom dimension"
    (let [d (c/add-dimension (d/acct-dataset 10)
                           :balance-bin [:bucket :balance 0 1000 10000])
          e (c/execute {:aggregate #{:balance-bin} :source d})
          e2 (c/execute {:aggregate #{} :source d})]
      (is (< 0 (count (:data e))))
      (is (= 10 (-> e2 :data first first))))))

(deftest test-file-dataset
  (testing "Query on "
    (let [s s/industry-source
          e (c/execute {:aggregate #{:sector} :source s})
          pa (c/page e)]
      (is (= 2 (count (:header pa))))
      (is (< 1 (count (:rows pa)))))))

;;TODO how to fail a cascalog query
(deftest test-failed-execution
  (testing "Failed execution with corrupted data"
    (let [d (update-in (d/acct-dataset 20) [:data]
                       (partial map #(dissoc % :balance)))
          e (c/execute {:aggregate #{:wrong} :source d})]
      (is (nil? (:error e))))))

(comment
  (def q2 {:aggregate #{:region} :filter {:type "A"} :source d2})
  (def d2 (c/add-dimension (d/acct-dataset 40) :bin-balance [:bucket :balance 0 5000 10000]))

  (def d1 (d/acct-dataset 40))
  (def q1 {:aggregate #{:region} :filter {:type "A"} :source d1})
  )
